# Defining instances of some recurrently used classes.
define heavenly_fade = Fade(0.5, 0.8, 0.5, color="#fff")

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define mc = Character("[persistent.mcname]", color = "#ffd700")
define gf = Character("Emiko", color = "#ffcccc")
define mm = Character("Anna", color = "#801d1d")
define os = Character("Jessica", color = "#4ba361")
define ys = Character("Julia", color = "#3d00bf")

# Setting a variable for controling played chapters

default next_chapter = 00
default persistent.supportmess_played = 0
default persistent.transitions_played = 0

# Setting default name for MC

default persistent.mcname = ""

# Intro sequence

label splashscreen:
    image menu_bg = "aa-general/title.png"
    image headphone = "aa-general/headphone.png"
    image mrrodrigues = "aa-general/mrrodrigues.png"
    image mcnamebg = "aa-general/name-bg.png"
    image intro = "aa-general/intro.png"
    image transitions = "aa-general/transitions.png"
        
    if persistent.mcname == "":
        show mcnamebg with Dissolve (1.5)
        with Pause (1)
        
        $ persistent.mcname = renpy.input("Please, choose the name of your character:\nLeave blank for \"Phil\".")
        $ persistent.mcname = persistent.mcname.strip()

        if persistent.mcname == "":
            $ persistent.mcname = "Phil"
        else:
            pass

        hide mcnamebg with Dissolve (1.5)
        with Pause (1)
    else:
        pass

    if persistent.transitions_played == 0:
        show transitions with Dissolve (1.5)
        with Pause (3)

        hide transitions with Dissolve (1.5)
        with Pause (1)

        show headphone with Dissolve (1.5)
        with Pause (3)

        hide headphone with Dissolve (1.5)
        with Pause (1)

        play sound "audio/intro.mp3"

        show intro with Dissolve (1.5)
        with Pause (10)

        hide intro with Dissolve (1.5)
        with Pause (1)

        show mrrodrigues with Dissolve (2)
        with Pause (2)

        hide mrrodrigues with Dissolve (2)
        with Pause (1)

        $ persistent.transitions_played = 1
    
        show menu_bg with Dissolve (5)
    else:
        play sound "audio/intro.mp3"

        show mrrodrigues with Dissolve (2)
        with Pause (2)

        hide mrrodrigues with Dissolve (2)
        with Pause (1)
    
        show menu_bg with Dissolve (2)

    return

# The game starts here

label start:

    # In here I'll control the game flow should the need arise for branching
    # chapters or wathever else there might be. Otherwise this will just work as
    # an index of sorts.

    if next_chapter == 00:
        jump prologue
    elif next_chapter == 01:
        jump chapter01
    else:
        jump end_credits
    return

label end_credits:

    image credits01 = "aa-general/credits-01.png"
    image credits02 = "aa-general/credits-02.png"
    image credits03 = "aa-general/credits-03.png"
    image credits04 = "aa-general/credits-04.png"
    image credits05 = "aa-general/credits-05.png"
    image credits06 = "aa-general/credits-06.png"

    stop sound fadeout 3.0
    stop music fadeout 3.0

    play music "audio/a-lonely-road.mp3" fadein 2.0
    
    show credits01 with Dissolve (1.5)
    with Pause (1.5)
    
    hide credits01 with Dissolve (1.5)
    with Pause (2)
    
    show credits02 with Dissolve (1.5)
    with Pause (5)
    
    hide credits02 with Dissolve (1.5)
    with Pause (2)
    
    show credits03 with Dissolve (1.5)
    with Pause (3)
    
    hide credits03 with Dissolve (1.5)
    with Pause (2)
    
    show credits04 with Dissolve (1.5)
    with Pause (3)
    
    hide credits04 with Dissolve (1.5)
    with Pause (2)

    show credits05 with Dissolve (1.5)
    with Pause (3)
    
    hide credits05 with Dissolve (1.5)
    with Pause (2)
    
    show credits06 with Dissolve (1.5)
    with Pause (5)
    
    hide credits06 with Dissolve (1.5)
    with Pause (2)

    jump postcredits

    return