### PROLOGUE

label prologue:
    
    ### SCENE 01
    ### Emiko finds out what is making MC feel sad.
    
    # Defining images used in this scene:

    image sc01_screen001 = "images/00-prologue/scene-01/screen-001.webp"
    image sc01_screen002 = "images/00-prologue/scene-01/screen-002.webp"
    image sc01_screen003 = "images/00-prologue/scene-01/screen-003.webp"
    image sc01_screen004 = "images/00-prologue/scene-01/screen-004.webp"
    image sc01_screen005 = "images/00-prologue/scene-01/screen-005.webp"
    image sc01_screen006 = "images/00-prologue/scene-01/screen-006.webp"
    image sc01_screen007 = "images/00-prologue/scene-01/screen-007.webp"
    image sc01_screen008 = "images/00-prologue/scene-01/screen-008.webp"
    image sc01_screen009 = "images/00-prologue/scene-01/screen-009.webp"
    image sc01_screen010 = "images/00-prologue/scene-01/screen-010.webp"
    image sc01_screen012 = "images/00-prologue/scene-01/screen-012.webp"
    image sc01_screen012 = "images/00-prologue/scene-01/screen-012.webp"
    image sc01_screen013 = "images/00-prologue/scene-01/screen-013.webp"
    image sc01_screen015 = "images/00-prologue/scene-01/screen-015.webp"
    image sc01_screen016 = "images/00-prologue/scene-01/screen-016.webp"
    image sc01_screen017 = "images/00-prologue/scene-01/screen-017.webp"

    stop sound fadeout 3.0
    with Pause (3)

    play music "audio/sad-acoustic-guitar-01.mp3" fadein 3.0

    "Sometimes life hits you."
    "Sometimes life hits you hard."
    gf "Honey, could you come to the kitchen for a second?"
    "A bad breakup, the loss of a job..."
    "...or the passing of a loved one."
    "And you cant help but feel lost, empty, or even broken."
    gf "Honey, please, I need your help over here!"
    "All you can hope to do, is to pick up the scattered pieces and try to put them back together."
    gf "..."
    "Hopefully, there will be someone there to help you along the way..."

    scene sc01_screen001
    gf "Honey, didn't you hear me?! I was..."

    scene sc01_screen002
    gf "Oh, I didn't mean to bother you,"
    gf "it's just that I was trying to fix us some dinner and..."

    scene sc01_screen003
    gf "..."
    gf "Sweetie, is everything okay? What happened?"

    scene sc01_screen004
    mc "It's my grandfather, he..."
    mc "I just recieved a call, he's..."
    mc "He passed away."

    scene sc01_screen005
    gf "Oh honey, I'm so sorry, I know how much he meant to you."
    mc "..."
    gf "..."

    scene sc01_screen006 with Dissolve (1)
    gf "How are you feeling, sweetie? Talk  to me..."

    scene sc01_screen007
    "At this point I do my best to fight the tears welling up inside me."

    scene sc01_screen008
    mc "I'm not okay..."
    mc "I can't make it in time for the funeral."
    mc "I won't even get a chance to say goodbye..."

    scene sc01_screen009
    gf "Honey... He knew how much you cared for him."
    gf "And I'm sure he's looking after you at this very moment."
    gf "Hoping that you can somehow feel better."

    scene sc01_screen010
    mc "You really think so?"
    gf "I know so..."

    scene sc01_screen012 #with heavenly_fade
    mc "You always know just what to say."
    mc "You remind me of him that way..."

    scene sc01_screen013 with Dissolve (1)
    gf "..."

    scene sc01_screen015
    gf "Tell you what, you go take a warm bath, and I'll go fix you a nice dinner."
    gf "It's not much, but a nice meal might make you feel a little better."

    scene sc01_screen016
    mc "Thank you, I love you babe..."
    scene sc01_screen017
    gf "I love you too honey..."

    ### SCENE 02
    ### Shower thoughts...
    
    # Defining images used in this scene:

    image sc02_screen001 = "images/00-prologue/scene-02/screen-001.webp"
    image sc02_screen002 = "images/00-prologue/scene-02/screen-002.webp"
    image sc02_screen003 = "images/00-prologue/scene-02/screen-003.webp"
    image sc02_screen004 = "images/00-prologue/scene-02/screen-004.webp"
    image sc02_screen005 = "images/00-prologue/scene-02/screen-005.webp"

    scene black with fade
    "While Emiko is preparing the dinner, I hop into the shower..."
    "On any given day, this would be my \"me time\", a time to put my head back in place."
    "To organize my thoughts."
    "But not today..."

    scene sc02_screen001 with fade
    mc "Fuck..."

    scene sc02_screen002
    mc "First dad, now grandpa is gone too..."
           
    scene sc02_screen003
    mc "They were the only ones that treated me like I was really family..."

    scene sc02_screen004
    mc "Unlike Anna..."
    mc "Her and that piece of shit that she brought home..."
    
    scene sc02_screen005
    mc "Fuck him, he got what he deserved..."

    scene sc02_screen003
    mc "I miss the girls though."

    scene sc02_screen002
    mc "God, it's been so long since I've last seen them..."

    scene black with fade
    mc "I wonder how they are doing nowadays..."

    "I finish my shower and head downstairs for dinner."
    "Emiko Is doing her best to cheer me up."
    "And I love her for it..."
    "But I can't say that I'm cooperating all that much."
    "In the end I spend most of the dinner silent..."
    "..."
    "God I'm such an idiot."
    "..."
    "Either way, after we are done I go upstairs to the bedroom."
    "All I want right now, Is a good night of sleep..."

    ### SCENE 03
    ### Emiko finds out what is making MC feel sad.
    
    # Defining images used in this scene:

    image sc03_screen001 = "images/00-prologue/scene-03/screen-001.webp"
    image sc03_screen002 = "images/00-prologue/scene-03/screen-002.webp"
    image sc03_screen003 = "images/00-prologue/scene-03/screen-003.webp"
    image sc03_screen004 = "images/00-prologue/scene-03/screen-004.webp"
    image sc03_screen005 = "images/00-prologue/scene-03/screen-005.webp"
    image sc03_screen006 = "images/00-prologue/scene-03/screen-006.webp"
    image sc03_screen007 = "images/00-prologue/scene-03/screen-007.webp"
    image sc03_screen008 = "images/00-prologue/scene-03/screen-008.webp"
    image sc03_screen009 = "images/00-prologue/scene-03/screen-009.webp"
    image sc03_screen010 = "images/00-prologue/scene-03/screen-010.webp"
    image sc03_screen011 = "images/00-prologue/scene-03/screen-011.webp"
    image sc03_screen012 = "images/00-prologue/scene-03/screen-012.webp"
    image sc03_screen013 = "images/00-prologue/scene-03/screen-013.webp"
    image sc03_screen014 = "images/00-prologue/scene-03/screen-014.webp"
    image sc03_screen015 = "images/00-prologue/scene-03/screen-015.webp"
    image sc03_screen016 = "images/00-prologue/scene-03/screen-016.webp"
    image sc03_screen017 = "images/00-prologue/scene-03/screen-017.webp"

    scene sc03_screen001
    with Dissolve (2)
    with Pause (4)

    scene sc03_screen002
    with Dissolve (2)
    with Pause (2)

    scene sc03_screen003
    with Dissolve (2)
    with Pause (1)

    scene sc03_screen004
    with Dissolve (2)
    with Pause (1)

    scene sc03_screen005
    with Dissolve (2)
    with Pause (1)

    scene sc03_screen006
    with Dissolve (2)
    with Pause (1)

    scene sc03_screen007
    with Dissolve (2)
    with Pause (1)

    scene sc03_screen008
    with Dissolve (2)
    with Pause (1)
    mc "..."

    scene sc03_screen009
    with Dissolve (2)
    gf "Hhmmmmm?..."

    scene sc03_screen010
    with Dissolve (2)
    mc "Sorry honey, did I wake you up?"

    scene sc03_screen011
    with Dissolve (2)
    gf "No... Well, yes, but it's ok, I'm just worried about you."

    scene sc03_screen012
    mc "It's all right babe, you can go back to sleep..."

    scene sc03_screen013
    with Dissolve (2)
    gf "No way, I'm here for you, what's bothering you?"

    scene sc03_screen014
    mc "I don't know how I feel about going home.."
    mc "I mean, I'm sad that I won't be able to say goodbye to granpa before the funeral,"
    mc "but I'm also kinda scared of meeting Anna and the girls again, it's been so long and we didn't part ways on the best terms..."
    gf "I see..."
    gf "Just one second, let me get the light."

    scene sc03_screen015 with heavenly_fade
    gf "Why?"
    mc "Why? Why what?"
    gf "Why are you scared?"
    mc "I don't know..."
    mc "I guess..."
    mc "I guess I'm still angry at what happened."
    mc "And if I'm angry, Anna must be as well, I mean, I'm no saint I fucked up bad. I know that now, but back then..."

    scene sc03_screen016
    gf "Back then you ran away from home, angry and alone..."
    gf "And only through sheer stubbornness you managed to be here today."
    gf "An accomplished and caring man."
    gf "If you managed to do so well, with all the odds  stacked against you,"
    gf "you'll manage to get through some awkward conversations just fine."
    mc "But if..."
    gf "No if, ands or buts. You will go home, and you will make ammends."
    gf "I've been keeping quiet about all of this for far too long."

    scene sc03_screen017
    gf "In fact, I'll go with you, and I'll be there by your side, every step of the way."
    mc "Yes ma'am..."
    gf "You're damn right, \"Yes ma'am\"!"
    gf "Now, since you oh so rudely woke me up..."

    scene black with fade

    play music "audio/equestrian-love.mp3" fadeout 5.0 fadein 3.0

    gf "How about we have some fun and take your mind off all of this?"

    ### SCENE 04
    ### Emiko finds out what is making MC feel sad.
    
    # Defining images used in this scene:

    image sc04_screen001 = "images/00-prologue/scene-04/screen-001.webp"
    image sc04_screen002 = "images/00-prologue/scene-04/screen-002.webp"
    image sc04_screen003 = "images/00-prologue/scene-04/screen-003.webp"
    image sc04_screen004 = "images/00-prologue/scene-04/screen-004.webp"
    image sc04_screen005 = "images/00-prologue/scene-04/screen-005.webp"
    image sc04_screen006 = "images/00-prologue/scene-04/screen-006.webp"
    image sc04_screen007 = "images/00-prologue/scene-04/screen-007.webp"
    image sc04_screen008 = "images/00-prologue/scene-04/screen-008.webp"
    image sc04_screen009 = "images/00-prologue/scene-04/screen-009.webp"
    image sc04_screen010 = "images/00-prologue/scene-04/screen-010.webp"
    image sc04_screen011 = "images/00-prologue/scene-04/screen-011.webp"
    image sc04_screen012 = "images/00-prologue/scene-04/screen-012.webp"
    image sc04_screen013 = "images/00-prologue/scene-04/screen-013.webp"
    image sc04_screen014 = "images/00-prologue/scene-04/screen-014.webp"
    image sc04_screen015 = "images/00-prologue/scene-04/screen-015.webp"
    image sc04_screen016 = "images/00-prologue/scene-04/screen-016.webp"
    image sc04_screen017 = "images/00-prologue/scene-04/screen-017.webp"
    image sc04_screen018 = "images/00-prologue/scene-04/screen-018.webp"
    image sc04_screen019 = "images/00-prologue/scene-04/screen-019.webp"
    image sc04_screen020 = "images/00-prologue/scene-04/screen-020.webp"
    image sc04_screen021 = "images/00-prologue/scene-04/screen-021.webp"
    image sc04_screen022 = "images/00-prologue/scene-04/screen-022.webp"
    image sc04_screen023 = "images/00-prologue/scene-04/screen-023.webp"
    image sc04_screen024 = "images/00-prologue/scene-04/screen-024.webp"
    image sc04_screen025 = "images/00-prologue/scene-04/screen-025.webp"
    image sc04_screen026 = "images/00-prologue/scene-04/screen-026.webp"
    image sc04_screen027 = "images/00-prologue/scene-04/screen-027.webp"
    image sc04_screen028 = "images/00-prologue/scene-04/screen-028.webp"
    image sc04_screen029 = "images/00-prologue/scene-04/screen-029.webp"
    image sc04_screen030 = "images/00-prologue/scene-04/screen-030.webp"

    # Defining videos used in this scene:
    image sc04_video001 = Movie(channel="any", play="images/00-prologue/scene-04/videos/video-001.webm")
    image sc04_video002 = Movie(channel="any", play="images/00-prologue/scene-04/videos/video-002.webm")
    image sc04_video003 = Movie(channel="any", play="images/00-prologue/scene-04/videos/video-003.webm")
    image sc04_video004 = Movie(channel="any", play="images/00-prologue/scene-04/videos/video-004.webm")
    image sc04_video005 = Movie(channel="any", play="images/00-prologue/scene-04/videos/video-005.webm")
    image sc04_video006 = Movie(channel="any", play="images/00-prologue/scene-04/videos/video-006.webm")

    $ renpy.start_predict("sc04_video00*")

    scene sc04_screen001 with fade
    gf "Now, let's wake this big fella up!"

    scene sc04_video001 with fade
    gf "Hmmmmmm, I love feeling it throb in my hand..."

    scene sc04_screen002
    gf "That's it, are you ready for me?"

    scene sc04_screen003
    mc "Goddamnit woman, anymore ready and I would be done!"

    scene sc04_video002 with fade
    gf "The hell you are, we are far from done!"

    scene sc04_screen004
    mc "Oh... fuck..."

    scene sc04_video003 with fade
    ""

    scene sc04_screen005
    mc "Damn babe..."

    scene sc04_screen006
    mc "Let me help you out..."

    scene sc04_screen007
    gf "Hmmmmmmm!"

    scene sc04_screen008
    with Dissolve (2)
    with Pause (1)

    scene sc04_screen009
    with Dissolve (2)
    with Pause (1)

    scene sc04_screen010
    with Dissolve (2)
    with Pause (1)

    scene sc04_screen011
    with Dissolve (2)
    with Pause (1)

    scene sc04_screen012
    with Dissolve (2)
    with Pause (1)

    scene sc04_screen013
    with Dissolve (2)
    with Pause (1)
    gf "Glph! Hnnnk!"

    scene sc04_screen014
    with Dissolve (2)
    gf "Hhhmmmmnnnnnnn..."
    mc "That's it babe..."

    scene sc04_screen015
    with Dissolve (2)
    mc "Just a little more..."
    gf "Glhk... Hnnnn... Hnnnn..."
    
    scene sc04_screen016
    gf "Cough! Cough! Ahhrrhhh..."

    scene sc04_screen017
    mc "Atta girl..."

    scene sc04_screen018
    gf "You're an asshole, you know that!"
    mc "That's why you love me, hehehe!"
    mc "Oh! You have a little something on your face honey..."

    scene sc04_screen019
    gf "Fuck you! Hehehe"
    mc "I'd much rather have YOU fuck ME!"

    scene sc04_screen020
    gf "Yeah, i'd like that too"
    gf "And it seems our friend is still up for it!"
    
    scene sc04_screen021 with fade
    gf "Fuck... I feel so full already..."

    scene sc04_screen022
    gf "Fuck me babe..."

    scene sc04_video004 with fade
    gf "Oh fuck... Oh fuck..."
    mc "That's it... ride me with your tight pussy!"

    scene sc04_screen023
    gf "Ahhhh..."
    gf "Damn you're so big..."
    gf "Fuck..."

    scene sc04_video005 with fade
    gf "I'll ride you nice and slow..."
    mc "That's it babe... ride my cock..."
    gf "Ahhh... Fuck! I love your cock so much!"

    scene sc04_screen024
    mc "Oh fuck... I'm gonna cum!"
    gf "I'm gonna cum too!"
    gf "Cum with me babe! Cum with me!"
    gf "Fill me up!"

    scene sc04_video006 with fade
    gf "Ahhhhh! Ahhhh!"
    gf "*moan* Fuuuuuu...."
    gf "..."

    scene sc04_screen025 with fade
    mc "Damn, that was awesome..."
    gf "Yes... it really was..."

    scene sc04_screen026
    gf "So, catch some \"Zs\" and then catch a flight?"
    mc "Well, I was thinking about catching myself some pussy!"

    scene sc04_screen027
    gf "Hehehe! You are insasiable!"
    mc "Damn right I am!"

    scene sc04_screen026
    gf "But honey, in all seriousness now, that's a long flight, and we didn't even pack yet."

    scene sc04_screen028
    mc "So what... We wake up late, pack and catch a late night flight..."
    mc "I think I can even get us an upgrade to executive! How does that sound?"
    gf "Well, when you put it like that..."
    mc "We can even sleep a little..."
    gf "Or have some airplane sex... Hehehe"

    scene sc04_screen029
    mc " Now look who's the insatiable one!"

    scene sc04_screen030
    mc "Now, bring that ass over here!"
    mc "I'm gonna eat it raw!"

    scene black with fade
    gf "Oh [persistent.mcname]!"

    $ renpy.stop_predict("*video*")

    $ next_chapter = 01
    $ post_credits = 01

    jump start

    return
