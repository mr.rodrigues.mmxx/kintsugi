### CHAPTER 01

label chapter01:

    ### SCENE 01
    ### Morning routine I

    # Defining images used in this scene:

    image chapter1 = "aa-general/chapter1.png"
    image saudade = "aa-general/saudade.png"
    image simeshift = "aa-general/timeshift.png"
    
    image ch01_sc01_screen001 = "images/01-ch01/scene-01/screen-001.webp"
    image ch01_sc01_screen002 = "images/01-ch01/scene-01/screen-002.webp"
    image ch01_sc01_screen003 = "images/01-ch01/scene-01/screen-003.webp"
    image ch01_sc01_screen004 = "images/01-ch01/scene-01/screen-004.webp"
    image ch01_sc01_screen005 = "images/01-ch01/scene-01/screen-005.webp"
    image ch01_sc01_screen006 = "images/01-ch01/scene-01/screen-006.webp"
    image ch01_sc01_screen007 = "images/01-ch01/scene-01/screen-007.webp"
    image ch01_sc01_screen008 = "images/01-ch01/scene-01/screen-008.webp"
    image ch01_sc01_screen009 = "images/01-ch01/scene-01/screen-009.webp"
    image ch01_sc01_screen010 = "images/01-ch01/scene-01/screen-010.webp"
    image ch01_sc01_screen011 = "images/01-ch01/scene-01/screen-011.webp"
    image ch01_sc01_screen012 = "images/01-ch01/scene-01/screen-012.webp"
    image ch01_sc01_screen013 = "images/01-ch01/scene-01/screen-013.webp"
    image ch01_sc01_screen014 = "images/01-ch01/scene-01/screen-014.webp"
    image ch01_sc01_screen015 = "images/01-ch01/scene-01/screen-015.webp"

    stop music fadeout 3.0
    stop sound fadeout 3.0
    with Pause (3)

    play music "audio/bedtime-song-alcaknight.mp3" fadein 3.0

    show chapter1 with Dissolve (1.5)
    with Pause (2)

    hide chapter1 with Dissolve (1.5)
    with Pause (1)

    show saudade with Dissolve (2)
    with Pause (15)

    hide saudade with Dissolve (2)
    with Pause (1)

    scene black with fade

    "We tend to think that dealing with death is an intense and brisk experience."
    "That we mourn our loved ones, pour or souls out and somehow emerge either healed or broken after some kind of esoteric experience."
    "But the fact is that is a much more mundane experience."
    "We don’t have days on end to devote to those we so dearly miss."


    "Not most of us at least..."

    scene ch01_sc01_screen001 with fade
    "More often than not, we get cast back in our routine way sooner than when we actually feel ready for it."

    scene ch01_sc01_screen006 with fade
    "Work, studies and responsibilities..."
    
    scene ch01_sc01_screen011 with fade
    "...Life."

    scene ch01_sc01_screen002 with fade
    "Life doesn’t care if you feel or even are ready."

    scene ch01_sc01_screen007 with fade
    "You just have to live it, and deal with it."

    scene ch01_sc01_screen012 with fade
    "Life is the biggest hindrance to actually dealing with death."

    scene ch01_sc01_screen003 with fade
    "It throws just enough shit your way to keep you distracted."

    scene ch01_sc01_screen008 with fade
    "So that you can deal with the pain, the longing and the melancholy at your own pace."

    scene ch01_sc01_screen013 with fade
    "And even if you feel like you might collapse under its weight..."

    scene ch01_sc01_screen004 with fade
    "...family will be there to pull you through."

    scene ch01_sc01_screen009 with fade
    os "..."

    scene ch01_sc01_screen014 with fade
    "Even if you don’t know who they are."

    scene ch01_sc01_screen005 with fade
    ys "Jess... are you almost done? I'm running a bit late..."

    scene ch01_sc01_screen010 with fade
    os "..."

    play audio "audio/sfx/door-bell-rings.flac"

    scene ch01_sc01_screen015 with fade
    with Pause (1)
    mm "Who on earth could it be at this hour?"

    scene black with fade
    "Even if you don’t realize it."
    mm "Coming!"

    scene timeshift with fade
    with Pause (1)

    ###################################################################################################
    
    ### SCENE 02
    ### At the airport
    
    # Defining images used in this scene:

    ## Backgrounds:
    image airp_ent = "images/ac-backgrounds/01-tokyo-airport/airport-entrance.png"
    image airp_dep = "images/ac-backgrounds/01-tokyo-airport/airport-departure.png"
    image airp_sho = "images/ac-backgrounds/01-tokyo-airport/airport-shopping.png"

    ## Emiko:
    image emiko 001 = "images/ab-characters/01-emiko/emiko-001.webp"
    image emiko 002 = "images/ab-characters/01-emiko/emiko-002.webp"
    image emiko 003 = "images/ab-characters/01-emiko/emiko-003.webp"
    image emiko 004 = "images/ab-characters/01-emiko/emiko-004.webp"
    image emiko 005 = "images/ab-characters/01-emiko/emiko-005.webp"
    image emiko 006 = "images/ab-characters/01-emiko/emiko-006.webp"
    image emiko 007 = "images/ab-characters/01-emiko/emiko-007.webp"
    image emiko 008 = "images/ab-characters/01-emiko/emiko-008.webp"
    image emiko 009 = "images/ab-characters/01-emiko/emiko-009.webp"
    image emiko 010 = "images/ab-characters/01-emiko/emiko-010.webp"
    image emiko 011 = "images/ab-characters/01-emiko/emiko-011.webp"
    image emiko 012 = "images/ab-characters/01-emiko/emiko-012.webp"
    image emiko 013 = "images/ab-characters/01-emiko/emiko-013.webp"
    image emiko 014 = "images/ab-characters/01-emiko/emiko-014.webp"
    image emiko 015 = "images/ab-characters/01-emiko/emiko-015.webp"
    image emiko 016 = "images/ab-characters/01-emiko/emiko-016.webp"
    image emiko 017 = "images/ab-characters/01-emiko/emiko-017.webp"
    image emiko 018 = "images/ab-characters/01-emiko/emiko-018.webp"
    image emiko 019 = "images/ab-characters/01-emiko/emiko-019.webp"
    image emiko 020 = "images/ab-characters/01-emiko/emiko-020.webp"
    

    stop music fadeout 3.0
    stop sound fadeout 3.0
    with Pause (3)

    play music "audio/sfx/busy-street-soft.ogg" fadein 3.0

    scene airp_ent

    show emiko 001 with Dissolve (1)
    gf "{size=+30}*Yawn*{/size}"

    show emiko 002
    gf "Oh wow... I'm sleepy, hehe..."
    mc "I noticed, with you sucking all the air around us."

    show emiko 003
    gf "Stop it... it wasn’t that bad!"
    mc "Wasn’t that bad?"
    mc "Honey, the great plague wasn’t that bad, you probably just killed thousands somewhere around the globe due to lack of oxygen..."

    show emiko 004
    mc "Poor bastards never even saw it coming..."

    show emiko 005
    gf "Stooooooop... You're embarassing me!"

    show emiko 005
    mc "Hehehe, ok... ok... I’ll stop."
    mc "You look cute when you blush!"

    show emiko 006
    gf "And you're an ass!"
    mc "Hehe... C’mon, let’s go inside and see if we can manage to catch the next flight."

    stop music fadeout 3.0
    stop sound fadeout 3.0

    play music "audio/sfx/busy-airport-lobby.ogg" fadein 5.0

    scene airp_dep with fade

    show emiko 007 with Dissolve (1)
    with Pause (1)
    gf "Oh, there’s a flight in about an hour from now, maybe we can make it?"
    mc "Hmmmmmm... Seems a little too soon, it’ll be tight."
    mc "Oh well, what the hell, let’s try it"

    show emiko 008
    gf "Oh, oh, oh..."

    show emiko 009
    gf "I have to get them something!"
    mc "Get who what?!"

    show emiko 010
    gf "Anna and the girls! I have to get them something, a little gift, a souvenir maybe..."
    mc "Babe, you never even met them, I don’t even know how well they will receive us."

    show emiko 011
    gf "All the more reason for me to get them something! I wanna make a good first impression! Besides, it’s impolite to show up empty-handed!"
    mc "Look, we are on a tight schedule here..."

    play audio ["audio/sfx/bing-bong.flac", "audio/sfx/japanese-airport-announcement.flac"]

    show emiko 012
    gf "Honey, they are your family, we have to get them something!"
    
    show emiko 013
    mc "No, not really, they aren’t. The only one that..."
    mc "..."

    show emiko 014
    mc "You are my real family."

    show emiko 015
    gf "Honey... Stop it..."
    gf "I mean, I love hearing you say that, and you know how much I love you."

    show emiko 014
    gf "But you have to stop it."

    show emiko 016
    gf "Otherwise you’ll grow old regretting never having realized how important family really is."

    show emiko 013
    mc "Even a foster one?"

    show emiko 003
    gf "..."

    show emiko 017
    gf "Look, I don’t wanna hear it right now, I’m gonna get the gifts and that’s final"

    show emiko 018 with Dissolve (1)
    mc "All right, all right... I’m going to get us the tickets and dispatch our bags."
    mc "You can go buy the gifts and then get us something to eat. And maybe a nice book."

    show emiko 019
    gf "Ok, meet me at the shopping area!"

    show emiko 020
    gf "See yah!"

    hide emiko with Dissolve (1)
    mc "Yeah, yeah..."
    mc "{size=-15}*mumble* *mumble*{/size}"

    $ next_chapter = 02
    $ post_credits = 01

    jump start

    return
