label postcredits:
    
    image postcredits_prologue_screen001 = "images/zz-postcredits/00-postcredits-prologue/screen-001.webp"
    image postcredits_prologue_screen002 = "images/zz-postcredits/00-postcredits-prologue/screen-002.webp"
    image postcredits_prologue_screen003 = "images/zz-postcredits/00-postcredits-prologue/screen-003.webp"
    image postcredits_prologue_screen004 = "images/zz-postcredits/00-postcredits-prologue/screen-004.webp"
    image postcredits_prologue_screen005 = "images/zz-postcredits/00-postcredits-prologue/screen-005.webp"
    image postcredits_prologue_screen006 = "images/zz-postcredits/00-postcredits-prologue/screen-006.webp"
    image postcredits_prologue_screen007 = "images/zz-postcredits/00-postcredits-prologue/screen-007.webp"
    
    scene black with fade
    with Pause (3)

    stop music fadeout 3.0

    play audio "audio/sfx/car-arriving-idling-and-pulling-away.flac"
    play audio "audio/sfx/urban-yard.flac"

    mc "Yup, that's it, you can pull up right over there."
    mc "Thanks, keep the change."
    
    play audio [ "<silence 5>", "audio/sfx/car-door-close.flac", "<silence .4>", "audio/sfx/car-door-slam.flac"]
    
    mc "Man... It hasn't changed a single bit..."
    mc "I feel like it was yesterdey since I last was here..."
    
    play audio "audio/sfx/walking-on-stone.flac"

    gf "Awww, how cute my big strong fiancee all nostalgic..."
    mc "Yeah... I'm not so sure this was a good idea..."
    gf "C'mon stop that, I'm here with you if you need me."
    mc "Man, she's gonna hate me for showing up like that."
    gf "Nonsense, ring the doorbell already!"
    
    play audio "audio/sfx/door-bell-rings.flac"

    gf "Here, I'll ring it for you..."
    mc "No, wait!"

    scene postcredits_prologue_screen002 with fade

    mm "Coming!"

    play audio "audio/sfx/door-open-close.flac"

    scene postcredits_prologue_screen003 with Dissolve (3)

    mm "Yes, how may I help you?"
    mc "Hi Anna..."

    scene postcredits_prologue_screen004
    mm "Oh my god..."
    mm "[persistent.mcname]?!?"


    scene postcredits_prologue_screen005
    mm "Girls, come quick!"
    os "What? ... Why?!"
    mm "Just come!"
    play audio ["audio/sfx/stairway-down-run.flac", "<silence .5>", "audio/sfx/door-closing.flac"]
    ys "Ok, just a second!"



    scene postcredits_prologue_screen006 with Dissolve (4)
    mm "Girls, [persistent.mcname] has come home..."

    scene postcredits_prologue_screen007 with Pause (3)

    scene black with fade
    "Well, I was not expecting that..."

    return